module gitlab.com/realtime-robotics/rtr-sigexec

go 1.13

require (
	github.com/ProtonMail/gopenpgp/v2 v2.2.4
	github.com/jessevdk/go-flags v1.5.0
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20211020174200-9d6173849985 // indirect
	golang.org/x/text v0.3.7 // indirect
)
