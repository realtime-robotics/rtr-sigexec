#!/bin/sh

# rtr-sigexec/debian/tests/hello-rtr-sigexec

set -euvx

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}[$$]: $*" >&2; }
error() { log "ERROR: $*"; }
warning() { log "WARNING: $*"; }
info() { log "INFO: $*"; }

die() {
    error "$@"
    exit 1
}

spit_vars() (
    for var; do info "${var}: $(eval echo "\${${var}}")"; done
)

################################################################################

export
info "Hello, $0!"
info "\$@:" "$@"
if ischroot; then
    info "ischroot: $?"
else
    warning "ischroot: $?"
fi

AUTOPKGTEST_NORMAL_USER="${AUTOPKGTEST_NORMAL_USER:-$(logname)}"
SUDO_USER="${SUDO_USER:-${AUTOPKGTEST_NORMAL_USER}}"
SUDO_UID="${SUDO_UID:-$(id -u "${SUDO_USER}")}"
SUDO_GID="${SUDO_GID:-$(id -g "${SUDO_USER}")}"
export AUTOPKGTEST_NORMAL_USER SUDO_USER SUDO_UID SUDO_GID
spit_vars AUTOPKGTEST_NORMAL_USER SUDO_USER SUDO_UID SUDO_GID

if ! command -v rtr-sigexec; then
    die "missing command: rtr-sigexec"
fi

self_test_dir="$(mktemp -dt self\ test\ dir.XXXXXX)"
readonly self_test_dir="${self_test_dir}"
info "$(chmod -v 0755 "${self_test_dir}")"
executable="${self_test_dir}/hello-rtr-sigexec"
readonly executable="${executable}"
cat >"${executable}" <<'EOF'
#!/bin/sh
set -euvx
env | sort
echo 'Hello, rtr-sigexec!'
exit "$?"
EOF

info "$(chmod -v 0755 "${executable}")"
readonly check_asc="${executable}.check.asc"
cat >"${check_asc}" <<'EOF'
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

SHA512 (hello-rtr-sigexec) = c20ed4122ecda04c3ac804fe9d2dfa6f1134f99a7075aa8bc2460f842a7f8024f02dfbf0722d9c0a7943b2ef73f1e7cd16df3f450044cc24273a79dd5a965b2f
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEE0jK/HFTg9LHx0RWSDC5RpqNJC5EFAl6XjBAACgkQDC5RpqNJ
C5E38A/8DYXz45irb1tM6Js+/hcksVo/3WpypLr6AybzSPcLEdizLL4gbMacWsiO
pqKJ4Fr4HooIocqGi74PlgbKOhEp74N5g5CH3QBTuAUZCHbLCWtezPTOID7NDGpW
cihkeTa5Bjr7ObQh1og2jJ74SmzgY1O2lhlvTB+lnZKDHvVngZz1Td1axZBthqd/
qu3W7RDHF2iIrG39Y372LsbLynUPTfJVGEr/mkQwzJ+ENTHIWhTgPegQyY70EceV
RPjpYW/Z44LVV8KZFc8SxQD56FgM3t+eXX4vy1ZkpgTByTaLEYp5srii4dl6SGI0
hit8K2Zkes82WNhWARM2IEKkYIbY51AShrjkCcg3xG/WUKEdYDgvBS5ZZhOeQVlB
/lQeURsx3KHvavbxb/U10VutYHx4LK5b412oerXmHlyUnxgNi94QdiJ3nIOOQYlN
KMK6Ug6mvAGbu87cjbXSAuE/15dXum35/coAZ5RUTtIqwHPNgAR2PsZI3r95iqEw
CMhT2l+wbqxDTfTdW5TqubvtAeKdI59ifjIJ2DfSFInAxPXs5A8OZD2otWrBmRtI
sjf53gohYAPjavohSX4Q4KI+gkwccGibXFd4hwIcy6ho3/7e6ajf+8V4HbSathap
x+bwL5zyiCoHaPhnClVnp6aL5eFuc2gCXmsqb3VKPP9B7eU91Po=
=KRFm
-----END PGP SIGNATURE-----
EOF
info "$(chmod -v 0644 "${check_asc}")"

rtr-sigexec "${executable}"
rtr-sigexec --dry-run "${executable}"
rtr-sigexec --dry-run --verbose "${executable}"
rtr-sigexec --verbose "${executable}"
rtr-sigexec -n "${executable}"
rtr-sigexec -n -v "${executable}"
rtr-sigexec -v "${executable}"

rm -rf "${self_test_dir}"

exit "$?"
